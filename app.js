var game = angular.module('game', ['ui.router', 'game.main', 'game.states', 'game.panel', 'game.login', 'game.register','game.edit']);

game.config(['$httpProvider', function($httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
}]);
