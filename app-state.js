angular.module('game.states', [])
    .config(function($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('main');

        $stateProvider

            .state('main', {
                url: '/',
                abstract: true,
                templateUrl: "index.html",
                controller: 'mainCtrl'
            })

            .state('login', {
                url: '/login',
                templateUrl: 'app/user/login/login.html',
                controller: 'loginCtrl'
            })

            .state('register', {
                url: '/register',
                templateUrl: 'app/user/register/register.html',
                controller: 'registerCtrl'
            })

            .state('edit', {
                url: '/edit',
                templateUrl: 'app/modules/edit/edit.html',
                controller: 'editCtrl'
            })

            .state('panel', {
                url: '/user-panel',
                templateUrl: 'app/user/panel.html',
                controller: 'panelCtrl'
            });
    });
