angular.module('game.main', [])
    .controller('mainCtrl', function($scope) {

        $scope.status = {};
        $scope.status.started = false;

        $scope.changeLogin = function() {
            $scope.status.started = true;
        }

    });
