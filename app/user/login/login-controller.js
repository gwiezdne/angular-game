angular.module('game.login', [])
    .controller('loginCtrl', function($scope, $http, $state) {

        $scope.login = {
            email: 'test@email.com',
            password: null,
            tryLogin: function() {
                data = {
                    email: $scope.login.email,
                    password: $scope.login.password
                };

                $http.post("https://api-platform-js.herokuapp.com/login", data)
                    .then(function(response) {
                        alert('nothing');
                        if (response.email !== null) {
                            alert('logged in');
                            $state.go(edit);
                        } else {
                            alert('wrong login or password');
                            $state.go(edit);
                        }
                    });
            }
        };

        $scope.guest = {
            name: null,
            oneOfUs: false,
            names: [
                'kaja',
                'wiewiorka',
                'monia',
                'monika',
                'krzysiek'
            ],
            testName: function() {
                if ($scope.guest.names.indexOf($scope.guest.name) !== -1) {
                    $scope.guest.oneOfUs = true;
                } else {
                    $scope.guest.oneOfUs = false;
                }
            }

        };


    });
